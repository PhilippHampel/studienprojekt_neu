0. Download und extrahieren des Studienprojekt.zip Ordners

1. Starten aller Anwendungen mit dem Kommando "docker-compose up"

	Info: Falls man das Docker compose Kommando zum zweiten mal ausführt dann zuerst die Kommandos

		docker stop /eureka-container

		docker rm /eureka-container

		docker stop /client-container-microservice1

		docker rm /client-container-microservice1

		docker stop /client-container-microservice2

		docker rm /client-container-microservice2

		docker stop /client-container-apigateway

		docker rm /client-container-apigateway

		docker stop /client-container-clientlookupservice

		docker rm /client-container-clientlookupservice

		docker-compose up -d --remove-orphans 

	ausführen und dann das "docker-compose up" Kommando

    Info: Alle Anwendungen benutzen den Actuator dieser kann über diese Links aufgerufen werden
        http://localhost:8761/actuator/health,
        http://localhost:1111/actuator/health,
        http://localhost:2222/actuator/health,
        http://localhost:3333/actuator/health,
        http://localhost:9191/actuator/health    
    

2. Unter http://localhost:8761/ läuft der Eureka Server, hier sind alle Mikroservices zu sehen

6. Unter http://localhost:1111/microservice1/test und unter http://localhost:2222/microservice2/test können die Endpunkte der zwei Microservices getestet werden

7. Unter http://localhost:3333/clientLookupMicroservice/clientLookupForMicroservice1 und unter http://localhost:3333/clientLookupMicroservice/clientLookupForMicroservice2 kann ein Client Lookup vom ClientLookupMicroservice zu Microservice1 oder Microservice2 ausgeführt werden

8. Unter 
    http://localhost:9191/microservice1/test oder
    http://localhost:9191/microservice2/test oder
    http://localhost:9191/clientLookupMicroservice/clientLookupForMicroservice1 oder
    http://localhost:9191/clientLookupMicroservice/clientLookupForMicroservice2
    können alle Microservices außer der Eureka Server auch durch das API Gateway erreicht werden


